<?php
if(!defined('HOST'))die('not access');
//系统配置文件		
return array(
	'url'		=> 'http://localhost/lyoa/',		//系统URL
	'localurl'	=> '',			//本地系统URL，用于服务器上浏览地址
	'title'		=> '李赢协同办公系统',	//系统默认标题
	'apptitle'	=> '李赢OA',			//APP上或PC客户端上的标题
	'db_host'	=> 'localhost',		//数据库地址
	'db_user'	=> 'root',		//数据库用户名
	'db_pass'	=> '',		//数据库密码
	'db_base'	=> 'lyoa',		//数据库名称
	'perfix'	=> 'lyoa_',	//数据库表名前缀
	'qom'		=> 'xinhu_',		//session、cookie前缀
	'highpass'	=> '',			//超级管理员密码，可用于登录任何帐号
	'db_drive'	=> 'mysqli',	//操作数据库驱动有mysql,mysqli,pdo三种
	'randkey'	=> 'bdsknxjcavrymfwuzpgohetiql',		//系统随机字符串密钥
	'asynkey'	=> '8464ba7b0e5cc7246058910fabb51212',	//这是异步任务key
	'openkey'	=> '6e3e4c84109780bb90969a7f1daf388a',	//对外接口openkey
	'sqllog'	=> false,		//是否记录sql日志保存upload/sqllog下
	'asynsend'	=> false,		//是否异步发送提醒消息，为true需开启服务端
	'install'	=> true			//已安装，不要去掉啊
);